const Validator = require('is-my-json-valid')
const isEqual = require('lodash.isequal')

module.exports = function Overwrite (opts = {}) {
  const {
    keyPattern = '.+',
    valueSchema = { type: 'string' }
  } = opts

  const identity = {}
  // const reifiedIdentity = {}

  const schema = {
    type: 'object',
    patternProperties: {
      [keyPattern]: valueSchema
    },
    additionalProperties: false
  }
  const isValid = Validator(schema, { verbose: true })

  function isIdentity (T) {
    return isEqual(T, identity)
  }

  function mapToOutput (T) {
    // if (isIdentity(T)) return reifiedIdentity
    return T
  }

  function concat (a, b) {
    if (!isValid(a)) {
      console.log(isValid.errors)
      throw ConcatError(a)
    }
    if (!isValid(b)) throw ConcatError(b)

    if (isIdentity(b)) return a

    return { ...a, ...b }
  }

  function mapFromInput (change) {
    if (typeof change !== 'object') throw new Error(`invalid input ${change}`)
    else {
      if (change === null) throw new Error('invalid input "null"')
      return change
    }
  }

  function isConflict (graph, tipIds, field, optSubFields, optResultRef = {}) {
    if (!Array.isArray(tipIds)) return new Error('tipIds should be an array')

    // Start with the tips of the graph and traverse backwards through history
    // For each branch record the first non-identity transform found and the nodeId.
    // (this is a "topTip")
    const topTips = {}

    // optResultRef[subField] can be used to store the final topTips

    for (const tipId of tipIds) {
      // If the overwrite fields are provided, use those, otherwise
      // - gather names of all fields named in history of each tipId
      const fields = (optSubFields || findSubFields(graph.getNode(tipId), graph, field))
      for (const subField of fields) {
      // find first nonIdentity mutation node
        const result = findFirstNonIdentityMutation(tipId, graph, field, subField, isIdentity)
        if (result) {
          // Store the first nonIdentity mutation for each subField in topTips
          if (!(subField in topTips)) topTips[subField] = { [tipId]: result }
          else topTips[subField][tipId] = result
        }
      }
    }
    // If different transforms are found, check if one is in the history of the other,
    // remove ones that are "lower" topTips
    Object.keys(topTips).forEach((subField) => {
      pruneTopTips(topTips, subField, graph)
    })
    // If there is a conflict in any overwrite field return true
    return Object.keys(topTips).some((subField) => {
      return findTopTipsConflict(topTips, subField, optResultRef, identity)
    })
  }

  function isValidMerge (graph, mergeNode, field) {
    isValidMerge.error = null
    isValidMerge.result = {}

    // First find what fields are not overridden by mergeNode.
    const subFields = findSubFields(mergeNode, graph, field)
    const mergeData = mergeNode.data
    if (field in mergeData) {
      Object.keys(mergeData[field]).forEach((overwrittenField) => {
        if (!isIdentity(mergeData[field][overwrittenField])) {
          isValidMerge.result[overwrittenField] = mergeData[field][overwrittenField]
          subFields.delete(overwrittenField)
        }
      })
    }
    const tipIds = mergeNode.previous
    const isItConflict = isConflict(graph, tipIds, field, subFields, isValidMerge.result)
    if (isItConflict) isValidMerge.error = new Error(`Field ${field} is causing a conflict that is not being merged`)
    return !isItConflict
  }

  function merge (graph, mergeNode, field) {
    if (isValidMerge(graph, mergeNode, field)) {
      return isValidMerge.result
    } else {
      throw MergeError(graph)
    }
  }

  return {
    schema,
    isValid,
    identity: () => identity,
    concat,
    mapFromInput,
    mapToOutput,
    isConflict,
    isValidMerge,
    merge
  }
}

function findSubFields (mergeNode, graph, field) {
  // - Build a set of the keys in the history of mergeNode
  const keyHistory = new Set(mergeNode.previous)
  for (const prevKey of mergeNode.previous) {
    for (const historyKey of graph.getHistory(prevKey)) {
      keyHistory.add(historyKey)
    }
  }
  // - gather names of all overwrite fields named in history of mergeNode
  const subFields = new Set()
  const mergeData = mergeNode.data
  if (field in mergeData) {
    Object.keys(mergeData[field]).forEach((subField) => { subFields.add(subField) })
  }

  Array.from(keyHistory).forEach(key => {
    const historyData = graph.getNode(key).data
    if (field in historyData) {
      Object.keys(historyData[field])
        .forEach((subField) => { subFields.add(subField) })
    }
  })
  return subFields
}

function findFirstNonIdentityMutation (tipId, graph, field, subField, isIdentity) {
  let result = {
    // nodeId = the id of the node that has mutated the field
    // T = the field transformation
  }

  const queue = [] // new Queue()
  queue.push(tipId)

  while (!('T' in result) && queue.length !== 0) {
    const nextId = queue.shift()
    const data = graph.getNode(nextId).data
    if (field in data && subField in data[field] && !isIdentity(data[field][subField])) {
      // Once a value is found, record the value and id.
      result = {
        nodeId: nextId,
        T: data[field][subField]
      }
    } else {
      // Otherwise add the previous Ids to the queue.
      for (const prevKey of graph.getPrevious(nextId)) {
        queue.push(prevKey)
      }
    }
  }

  if (!('T' in result || isIdentity(result.T))) return
  // no mutation found in this tips history

  return result
}

function pruneTopTips (topTips, subField, graph) {
  // For each of the topTips, test if it is in the history of other branches
  Object.entries(topTips[subField]).forEach(([branchID, branchValue]) => {
    Object.entries(topTips[subField]).forEach(([otherID, otherValue]) => {
      if (branchID === otherID) return
      if (graph.getHistory(otherID).includes(branchValue.nodeId)) {
        delete topTips[subField][branchID]
      }
    })
  })
}

function findTopTipsConflict (topTips, subField, optResultRef, identity) {
  if (Object.values(topTips[subField]).length === 0) {
    optResultRef[subField] = identity
    return false
  }
  const result = Object.values(topTips[subField])[0].T
  optResultRef[subField] = result

  if (Object.values(topTips[subField]).length === 1) return false
  // Otherwise, check if the tips are different
  return (Object.values(topTips[subField]).some(function (tip) {
    return !isEqual(result, tip.T)
  }))
}

function ConcatError (T) {
  return new Error(`cannot concat invalid transformation ${JSON.stringify(T)}`)
}
function MergeError (T) {
  return new Error(`cannot merge invalid transformations ${JSON.stringify(T)}`)
}
