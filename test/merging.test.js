const test = require('tape')
const tangle = require('@tangle/test')

const Strategy = require('../')
const {
  identity,
  isConflict,
  isValidMerge,
  merge
} = Strategy()

test('isConflict', t => {
  // merging stuff! ///////////////////////////
  const buildGraph = (mermaid) => tangle.buildGraph(mermaid, { dataField: 'complex' })
  const branchGraph = buildGraph(`
    A-->B
    A-->C
  `)
  const A = branchGraph.getNode('A')
  A.data.complex.first = '1'
  A.data.complex.second = '2'
  t.equal(isConflict(branchGraph, ['B', 'C'], 'complex'), false, 'values only on root')

  const B = branchGraph.getNode('B')
  B.data.complex.first = 'One'
  B.data.complex.second = 'Two'
  t.equal(isConflict(branchGraph, ['B', 'C'], 'complex'), false, 'values on root and one branch')

  const C = branchGraph.getNode('C')
  C.data.complex.second = 'Two'
  t.equal(isConflict(branchGraph, ['B', 'C'], 'complex'), false, 'Comparison shows no difference or identity')
  C.data.complex.first = '1'
  t.equal(isConflict(branchGraph, ['B', 'C'], 'complex'), true, 'one conflicts')

  t.end()
})

test('isValidMerge', t => {
  // merging stuff! ///////////////////////////
  const buildGraph = (mermaid) => tangle.buildGraph(mermaid, { dataField: 'complex' })
  const graph = buildGraph(`
    A-->B-->D
    A-->C-->D
  `)
  const mergeNode = graph.getNode('D')

  const A = graph.getNode('A')
  A.data.complex.first = '1'
  A.data.complex.second = '2'

  const B = graph.getNode('B')
  B.data.complex.first = 'One'
  B.data.complex.second = 'Two'

  const C = graph.getNode('C')
  C.data.complex.second = 'Two'

  t.true(isValidMerge(graph, mergeNode, 'complex'), 'identity merge works for no conflict')

  C.data.complex.first = '1'
  t.false(isValidMerge(graph, mergeNode, 'complex'), 'identity merge doesnt work for conflict')

  mergeNode.data.complex.second = 'Sec'
  t.false(isValidMerge(graph, mergeNode, 'complex'), 'Merge node doesnt resolve conflicting field')

  mergeNode.data.complex.third = '3'
  t.false(isValidMerge(graph, mergeNode, 'complex'), 'New field only in merge node doesnt cause error')

  mergeNode.data.complex.first = 'Fir'
  t.true(isValidMerge(graph, mergeNode, 'complex'), 'Overwrites conflicting value')

  t.end()
})

test('simple merge', t => {
  // merging stuff! ///////////////////////////
  // Same example as above, building towards a conflicting graph on one of the fields
  const buildGraph = (mermaid) => tangle.buildGraph(mermaid, { dataField: 'complex' })
  const graph = buildGraph(`
    A-->B-->D
    A-->C-->D
  `)
  const mergeNode = graph.getNode('D')

  const A = graph.getNode('A')
  A.data.complex.first = '1'
  A.data.complex.second = '2'

  const B = graph.getNode('B')
  B.data.complex.first = 'One'
  B.data.complex.second = 'Two'

  const C = graph.getNode('C')
  C.data.complex.second = 'Two'

  t.deepEqual(merge(graph, mergeNode, 'complex'), { first: 'One', second: 'Two' }, 'identity merge works for no conflict')

  C.data.complex.first = '1'
  t.throws(() => merge(graph, mergeNode, 'complex'), /cannot merge invalid transformations/, 'identity merge doesnt work for conflict')

  mergeNode.data.complex.second = 'Sec'
  t.throws(() => merge(graph, mergeNode, 'complex'), /cannot merge invalid transformations/, 'Merge node doesnt resolve conflicting field')

  mergeNode.data.complex.third = '3'
  t.throws(() => merge(graph, mergeNode, 'complex'), /cannot merge invalid transformations/, 'New field only in merge node doesnt resolve error')

  mergeNode.data.complex.first = 'Fir'
  t.deepEqual(merge(graph, mergeNode, 'complex'), { first: 'Fir', second: 'Sec', third: '3' }, 'Overwrites conflicting value')

  B.data.complex.forth = 4
  t.deepEqual(merge(graph, mergeNode, 'complex'), { first: 'Fir', second: 'Sec', third: '3', forth: 4 }, 'New field only in branch node')

  t.end()
})

test('merging (complex)', t => {
  // Two branches (A and C), then merge three tips
  const buildGraph = (mermaid) => tangle.buildGraph(mermaid, { dataField: 'title' })
  const graph = buildGraph(`
    A-->B------>M
    A-->C-->D-->M
        C-->E-->M
   `)
  let mergeNode = graph.getNode('M')

  t.deepEqual(merge(graph, mergeNode, 'title'), identity(), 'merge identities')

  const A = graph.getNode('A')
  A.data.title.animal = 'dog'
  t.deepEqual(merge(graph, mergeNode, 'title'), { animal: 'dog' }, 'only root has title')

  const C = graph.getNode('C')
  C.data.title.animal = 'cat'
  t.deepEqual(merge(graph, mergeNode, 'title'), { animal: 'cat' }, 'title override in C')

  const D = graph.getNode('D')
  D.data.title.animal = 'mouse'
  t.deepEqual(merge(graph, mergeNode, 'title'), { animal: 'mouse' }, 'Tip overwrites earlier branches')

  const E = graph.getNode('E')
  E.data.title.object = 'fridge'
  t.deepEqual(merge(graph, mergeNode, 'title'), { animal: 'mouse', object: 'fridge' }, 'Other nodes dont conflict on other fields')

  E.data.title.animal = 'fridge'
  t.throws(() => merge(graph, mergeNode, 'title'), /cannot merge invalid transformations/, 'do conflict on animal')

  // Two merge nodes E and M in sequence
  const twoMerge = buildGraph(`
   A-->B[{ t: 'cats' }]->D-->M
   A-->C[{ t: 'dogs' }]->F-->M
       B------------------>E-->M
       C------------------>E
    `)
  mergeNode = twoMerge.getNode('E')

  t.equal(isConflict(twoMerge, ['B', 'C'], 'title'), true, 'Conflict resolving E')
  t.equal(isValidMerge(twoMerge, mergeNode, 'title'), false, 'Conflict resolving E')
  t.throws(() => { throw isValidMerge.error }, /Field title is causing a conflict/, 'isValidMerge.error is set')
  t.throws(() => merge(twoMerge, mergeNode, 'title'), /cannot merge invalid transformations/, 'E is invalid merge')
  // t.throws(() => merge(twoMerge, 'M', 'title'), /cannot merge invalid transformations/,
  // 'invalid merge earlier in graph is not caught')  //Actual result: undefined

  mergeNode.data.title = { t: 'either' }
  t.deepEqual(merge(buildGraph(`
  A-->B[{ t: 'cats' }]->D-->M
  A-->C[{ t: 'dogs' }]->F-->M
      B------------------>E-->M
      C------------------>E[{ t: 'either' }]
   `), mergeNode, 'title'), { t: 'either' }, 'earlier merge')

  mergeNode = twoMerge.getNode('M')
  t.deepEqual(merge(buildGraph(`
  A-->B[{ t: 'cats' }]->D-->M
  A-->C[{ t: 'dogs' }]->F-->M
      B------------------>E-->M
      C------------------>E[{ t: 'either' }]
   `), mergeNode, 'title'), { t: 'either' }, 'stacked merges')

  const middleX = buildGraph(`
  A-->B-->D[{ t: 'left' }]--->G
      B-->E-------------------->G-->I
  A-->C-->E-------------------->H-->I
      C-->F[{ t: 'right' }]-->H
   `)
  t.deepEqual(merge(middleX, middleX.getNode('G'), 'title'), { t: 'left' }, 'left merge')
  t.deepEqual(merge(middleX, middleX.getNode('H'), 'title'), { t: 'right' }, 'right merge')
  t.throws(() => merge(middleX, middleX.getNode('I'), 'title'), /cannot merge invalid transformations/, 'left and right conflict')
  t.end()
})
